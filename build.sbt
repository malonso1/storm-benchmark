name := "StormProject"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies += "org.apache.storm" % "storm-core" % "0.10.0"

libraryDependencies += "org.apache.storm" % "storm-kafka" % "0.10.0"

libraryDependencies += "org.apache.kafka" % "kafka_2.10" % "0.9.0.0"

libraryDependencies += "com.fasterxml.jackson.core" % "jackson-databind" % "2.7.3"