import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;

import java.io.IOException;
import java.util.Map;

import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Created by marc on 28/03/2016.
 */
public class LogParserBolt implements IRichBolt {

    private OutputCollector collector;
    private ObjectMapper objectMapper = new ObjectMapper();

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.collector = collector;
    }

    @Override
    public void execute(Tuple input) {
        String json = input.getString(0);
        try {
            WebLog log = objectMapper.readValue(json, WebLog.class);
            this.collector.emit(new Values(log));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    @Override
    public void cleanup() {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("webLog"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
