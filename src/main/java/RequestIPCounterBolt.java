import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichBolt;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Values;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by marc on 28/03/2016.
 */
public class RequestIPCounterBolt implements IRichBolt {

    private OutputCollector collector;
    Map<String, Integer> counterMap;

    @Override
    public void prepare(Map stormConf, TopologyContext context, OutputCollector collector) {
        this.counterMap = new HashMap<String, Integer>();
        this.collector = collector;
    }

    @Override
    public void execute(Tuple input) {
        WebLog ip = (WebLog) input.getValue(0);

        if(!counterMap.containsKey(ip.iprequeteur)){
            counterMap.put(ip.iprequeteur, 1);
        }else{
            Integer c = counterMap.get(ip.iprequeteur) + 1;
            counterMap.put(ip.iprequeteur, c);
        }
        collector.ack(input);
    }

    @Override
    public void cleanup() {
        for(Map.Entry<String, Integer> entry : counterMap.entrySet()){
            System.out.println(entry.getKey()+" : " + entry.getValue());
        }
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("count"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }
}
