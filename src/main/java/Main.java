import backtype.storm.Config;
import backtype.storm.LocalCluster;
import backtype.storm.scheduler.Cluster;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Tuple;
import storm.kafka.KafkaSpout;
import storm.kafka.SpoutConfig;
import storm.kafka.ZkHosts;

import java.util.Map;
import java.util.UUID;

/**
 * Created by marc on 28/03/2016.
 */
public class Main {

    public static class KafkaDisplayerBolt extends BaseRichBolt {
        OutputCollector _collector;

        @Override
        public void prepare(Map conf, TopologyContext context, OutputCollector collector) {
            _collector = collector;
        }

        @Override
        public void execute(Tuple tuple) {
            System.out.println(tuple.getString(0));
            _collector.ack(tuple);
        }

        @Override
        public void declareOutputFields(OutputFieldsDeclarer declarer) {
        }
    }

    public static void main(String[] args) throws Exception{
        ZkHosts hosts = new ZkHosts("localhost:2181");
        SpoutConfig spoutConfig = new SpoutConfig(hosts, "log-events", "/log-events", UUID.randomUUID().toString());
        KafkaSpout kafkaSpout = new KafkaSpout(spoutConfig);

        Config config = new Config();
        config.setDebug(true);

        TopologyBuilder builder = new TopologyBuilder();

        builder.setSpout("kafka-spout", kafkaSpout, 1);

        builder.setBolt("kafka-display-bolt", new KafkaDisplayerBolt())
            .shuffleGrouping("kafka-spout");

        /*builder.setSpout("log-spout", new LogSpoutScala());

        builder.setBolt("log-parser-bolt", new LogParserBolt())
            .shuffleGrouping("log-spout");

        builder.setBolt("request-counter-bolt", new RequestIPCounterBolt())
                .shuffleGrouping("log-parser-bolt");*/

        LocalCluster cluster = new LocalCluster();
        cluster.submitTopology("logParser", config, builder.createTopology());

        Thread.sleep(100000);
        cluster.shutdown();
    }
}
