import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.IRichSpout;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.util.Map;
import java.util.Random;

/**
 * Created by marc on 28/03/2016.
 */
public class LogSpout implements IRichSpout {

    private SpoutOutputCollector collector;
    private boolean completed = false;
    private TopologyContext context;
    private Random randomGenerator = new Random();

    @Override
    public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.context = context;
        this.collector = collector;
    }

    @Override
    public void close() {

    }

    @Override
    public void activate() {

    }

    @Override
    public void deactivate() {

    }

    @Override
    public void nextTuple() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        int randomInt = randomGenerator.nextInt(20);
        String rndlog = "{" +
                "\"ipserveur\" : \"192.168.98.45\",\n" +
                "\"pageweb\" : \"GET /apache_pb.gif HTTP/1.0\",\n" +
                "\"iprequeteur\" : \"192.168.98." + randomInt + "\",\n" +
                "\"idsession\" : \"9818b8e2-6449-4d02-813a-8516fae8ec1a\",\n" +
                "\"tempsdereponse\" : 50,\n" +
                "\"timestampRequete\" : 1453999085,\n" +
                "\"codeHTTPRetour\" : 404" +
            "}";
        this.collector.emit(new Values(rndlog));
    }

    @Override
    public void ack(Object msgId) {

    }

    @Override
    public void fail(Object msgId) {

    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
        declarer.declare(new Fields("jsonLog"));
    }

    @Override
    public Map<String, Object> getComponentConfiguration() {
        return null;
    }

    public boolean isDistributed() {
        return false;
    }
}
