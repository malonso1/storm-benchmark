import java.util
import java.util.Random

import backtype.storm.spout.SpoutOutputCollector
import backtype.storm.task.TopologyContext
import backtype.storm.topology.{OutputFieldsDeclarer, IRichSpout}
import backtype.storm.tuple.{Values, Fields}

import scala.util.Try

/**
  * Created by marc on 29/03/2016.
  */
class LogSpoutScala extends IRichSpout {
  private var collector: SpoutOutputCollector = null
  private var completed: Boolean = false
  private var context: TopologyContext = null
  private var randomGenerator: Random = new Random

  override def nextTuple(): Unit = {
    Try(Thread.sleep(20))
    val randomInt: Int = randomGenerator.nextInt(20)
    val rndlog: String = "{" + "\"ipserveur\" : \"192.168.98.45\",\n" + "\"pageweb\" : \"GET /apache_pb.gif HTTP/1.0\",\n" + "\"iprequeteur\" : \"192.168.98." + randomInt + "\",\n" + "\"idsession\" : \"9818b8e2-6449-4d02-813a-8516fae8ec1a\",\n" + "\"tempsdereponse\" : 50,\n" + "\"timestampRequete\" : 1453999085,\n" + "\"codeHTTPRetour\" : 404" + "}"
    this.collector.emit(new Values(rndlog))
  }

  override def activate(): Unit = {}

  override def deactivate(): Unit = {}

  override def close(): Unit = {}

  override def fail(msgId: scala.Any): Unit = {}

  override def open(conf: util.Map[_, _], context: TopologyContext, collector: SpoutOutputCollector): Unit = {
    this.context = context
    this.collector = collector
  }

  override def ack(msgId: scala.Any): Unit = {}

  override def getComponentConfiguration: util.Map[String, AnyRef] = null

  override def declareOutputFields(declarer: OutputFieldsDeclarer): Unit = declarer.declare(new Fields("jsonLog"))

  def isDistributed: Boolean = false
}
